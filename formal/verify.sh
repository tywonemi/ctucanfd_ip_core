#!/bin/bash

set -ex

test -d public
pushd public
SBY_FILE=formal/template.sby
sed "s/FILES_SPACES/$(cat rtl_lst.txt | sed 's/rtl\///g' | tr '\n' ' ')/" $SBY_FILE | cat - rtl_lst.txt > formal/verify.sby
sby --yosys="yosys -m ghdl" formal/verify.sby safety -f
sby --yosys="yosys -m ghdl" formal/verify.sby cover -f
popd
